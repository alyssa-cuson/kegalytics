var FlowMeter = require('./flowmeter'),
    PinMonitor = require('./pinmonitor'),
    util = require('util'),
    EventEmitter = require('events').EventEmitter;

var POUR_TIMEOUT = 2500;

var Tap = function (pin) {
  this.meter = new FlowMeter();
  this.monitor = new PinMonitor(pin);

  EventEmitter.call(this);

  // When we receive a pulse, send it to the meter
  this.monitor.on('rising', this.update.bind(this));
};
util.inherits(Tap, EventEmitter);

var isPouring = false;

Tap.prototype.update = function () {
  if (this.meter.pour === 0 && isPouring === false) {
    isPouring = true;
    this.emit('pour:start');
  }

  // send update to the meter
  var currentTime = new Date().getTime();
  this.meter.update(currentTime);

  this.emit('pour:progress', { flow: this.meter.getFlow(), cumulativeFlow: this.meter.getPour() });
  if (this.timeout) {
    clearTimeout(this.timeout);
  }
  this.timeout = setTimeout(function () {
    this.emit('pour:end', this.meter.getPour());
    this.meter.clear();
    isPouring = false;
  }.bind(this), POUR_TIMEOUT);
};

module.exports = Tap;

