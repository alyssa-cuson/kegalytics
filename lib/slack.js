//Require Slack Web API package
const { WebClient } = require('@slack/web-api');

//Welcome message
console.log('Kegalytics says hi! 👋')

//Env variable for the Slack Token
const web = new WebClient(process.env.SLACK_TOKEN);

//Function for posting a message
(async () => {

    try {
      // Use the `chat.postMessage` method to send a message from this app
      await web.chat.postMessage({
        channel: '#kegalytics-slack-test',
        text: `It's 5 o'clock somewhere 🍻`,
      });
      console.log('Message posted! 🚀');
    } catch (error) {
      console.log(error);
    }
  
  });

 module.exports = slack; 