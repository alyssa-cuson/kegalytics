var Gpio = require('onoff').Gpio;

function PinMonitor (pin) {
  this.pin = pin;
}

PinMonitor.prototype.on = function(on, cb) {
  this._monitor = new Gpio(this.pin, 'in', on);

  this._monitor.watch(function (err) {
    if (err) {
      console.error(err);
      exit(this._monitor);
    }
    cb();
  });

  process.on('SIGINT', exit.bind(null, this._monitor));
};

function exit (monitor) {
  monitor.unexport();
  process.exit();
}

module.exports = PinMonitor;
