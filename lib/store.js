const sqlite = require('sqlite')
const kegSchema = [
  {name: "id", type: "INTEGER PRIMARY KEY AUTOINCREMENT"},
  {name: "name", type: "TEXT"},
  {name: "breweryName", type: "TEXT"},
  {name: "pours", type: "INTEGER"},
  {name: "active", type: "BOOLEAN default true"},
  {name: "breweryLocation", type: "TEXT"},
  {name: "type", type: "TEXT"},
  {name: "abv", type: "REAL"},
  {name: "volume", type: "INTEGER"},
  {name: "remaining", type: "INTEGER"},
  {name: "cost", type: "REAL"},
  {name: "color", type: "INTEGER"},
]

const store = (db) => ({
  all: () => db.all('SELECT * FROM keg'),
  get: (id) => db.get('SELECT * FROM keg WHERE id = ?', id),
  set: (id, data) => {
    return db.all('SELECT * FROM keg WHERE id = ?', id)
    .then((records) => {
      const record = records[0] || {id}
      const fields = kegSchema.map(({name}) => name)
      const newData = {...record, ...data}
      const newRecord = fields.reduce((acc, field) => {
        return {...acc, [`$${field}`]: newData[field]}
      }, {})
      return db.run(
        `INSERT OR REPLACE INTO keg (${fields.join(', ')}) values(${Object.keys(newRecord).join(', ')})`,
        newRecord
      )
    })
  }
})

module.exports = (file) => {
  const database = sqlite.open(file, { Promise })
  const schema = kegSchema.map(({name, type}) => `${name} ${type}`)
  return database.then(db => {
    return db.run(`CREATE TABLE IF NOT EXISTS keg (${schema.join(', ')})`)
    .then(() => store(db).set(0, {name: 'default1', active: true}))
    .then(() => store(db).set(1, {name: 'default2', active: true}))
    .then(() => store(db))
  })
}
