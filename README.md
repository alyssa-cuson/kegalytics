# Kegalytics

## Installation

    npm install
    bower install

# Pin Setup

Currently, it's configured to work with GPIO's 2 and 14, based on the rainbow cable configuration.

GPIO 2 - Pin 3 - Rainbow Tap (in the UI)
GPIO 14 - Pin 8 - White Tap

# Running It

It requires some ENV vars to know where/what it is:

`sudo OS=pi NODE_ENV=production DEBUG=kegalytics npm start`

# Running it on the server
 - SDI Atlanta Office has kegalytics pinned to IP Address: 192.168.10.105
 - `ssh 10.20.1.24` and enter the password
 - `cd kegalytics`
 - `sudo OS=pi NODE_ENV=production forever start -l log.log -o out.log -e err.log -a bin/www` (the sudo password is the same as the ssh password)

# Initial Setup

The `store.sqlite` file needs to be initialized in order to load most of the views. To do that, go to:

`http://kegalytics.ip.address:3000/setup`

Here you can input the keg information and then everything should load fine the next time.

# Hardware List

TODO: Lay out the hardware we need for the project, in case it goes kaput again.
