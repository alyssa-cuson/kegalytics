var express = require('express');
var router = express.Router();

function parseQuery (query) {
  for (var key in query) {
    query[key] = parseQueryString(query[key]);
  }
  return query;
}

function parseQueryString (str) {
  if (str === 'true') {
    return true;
  }
  if (str === 'false') {
    return false;
  }
  if (isNumber(str)) {
    return parseFloat(str);
  }
  return str;
}

function isNumber(n) {
  return !isNaN(parseFloat(n)) && isFinite(n);
}

/* GET home page. */
router.get('/', function(req, res) {
  req.store.all().then(function (kegs) {
    res.render('index', { kegs: kegs });
  });
});

router.get('/kegs/:id', function(req, res) {
  var kegs = [];
  kegs[req.params.id] = parseQuery(req.query);
  res.render('keg', { num: req.params.id, kegs: kegs });
});

router.get('/setup', function (req, res) {
  req.store.all().then(function (kegs) {
    res.render('admin', { kegs: kegs });
  });
});

router.post('/setup/kegs/:id', function (req, res) {
  var id = req.params.id,
      kegData = req.body;

  kegData.active = kegData.active === 'on';
  kegData.id = id;

  kegData = parseQuery(kegData);

  req.store.set(id, kegData).then(function (keg) {
    req.io.emit('keg update', kegData);
    res.send(200);
  }, function (err) {
    console.log(err);
  });
});

module.exports = router;
